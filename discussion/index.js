// Conditional Statements - allows us to control the flow of our program.

// [SECTION] if, else if, else


/*
Syntax:

	if(condition){
	statement
	};
*/

// Example:
// if Statement
let numA = 5;

// if(numA < 3) di lalabas kasi false or mali.
if(numA > 3) {
	console.log('Hello');	
}

// Example:
let city = "New York";

if(city === "New York"){
	console.log("Welcome to New York City!");
}

// else if Clause

/*
	- Executes a statement if previous condition are false and if the specified condition is true.
	- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.
*/

// else if() statement was no longer run because the if statement was able to run and the evaluation of the whole statement stops there.

let numB = 1;

if(numA > 3){ // numA = 5 // true
	console.log('Hello');
} else if (numB > 0) {     // else if is a back up. // true 
	console.log('World');
}

/*^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^-^*/
if(numA < 3){ // numA = 5 // false
	console.log('Hello');
} else if (numB > 0) {     // else if is a back up. // true 
	console.log('World');
}




/*--------------------------------------------------------------*/
// else if in String
city = "Tokyo";


if(city === "New York"){
	console.log("Welcome to New York City!");
} else if (city === "Tokyo"){
	console.log("Welome to Tokyo, Japan!");
}

/*--------------------------------------------------------------*/
// else Statement
/*
	- Executes a statement if all other condition are false 
	- The "else" statement is optional  and can be added to capture any other result to change the flow of a program.
*/
let numC = -5;
let numD = 7;

if(numC > 0){ // false
	console.log('Hello');
} else if (numD === 0) { // false
	console.log('World');
} else {
	console.log('Error');
}

/*Pwede rin wala or meron. Para makita yung error.

 else {
	console.log('Error');
*/
/*----------------------------------------------------------------*/

// if, else if and else statement with function.
// > greater than, < less than, = equals.


function determineTyphoonIntensity(windSpeed) {

	if (windSpeed < 30) {
		return 'Not a typhoon yet.';
	}
	else if (windSpeed <= 61) {
		return 'Tropical depression detected.';
	}
	else if (windSpeed >= 62 && windSpeed <= 88) { // && means AND operator. Dapat tumama ang 62 and 88 para maging True.
		return 'Tropical storm detected.';
	}
/*	else if (windSpeed >= 89 || windSpeed <= 117) { // || means OR operator
		return 'Severe Tropical Storm detected.'*/

		else if (windSpeed >= 89 && windSpeed <= 117) { // || means OR operator
		return 'Severe Tropical Storm detected.'
	}
	else {
		return 'Typhoon detected.';
	}
}


let message = determineTyphoonIntensity(87);
console.log(message);


/*------------------------------------------------------*/

if (message == 'Tropical storm detected.') {
	console.warn(message)
}
/*-------------------------------------------------------*/

// [SECTION] Conditional (Ternary) Operator
/*
	- The conditional (Ternary) Operator takes in three operands:
	1. condition
	2. expression to execute if the condition is truthy
	3. expression to execute if the condition is falsy
	
	// Ternary operator is for shorthand code - means commonly used for single statement execution where the result consist  of only one line of code.

	- Syntax
		(expression) ? ifTrue : ifFalse;


		- Can be used as an alternative to an "if else" statement.
*/

/* Single statement execution

We can also use this one.
 for example:
	let t = "yes";
	let f = "no";

let ternaryResult = (1 < 18) ? t : f
console.log("result of Ternary Operator: " + ternaryResult);

*/

// Single statement execution
let ternaryResult = (1 < 18) ? true : false
console.log("result of Ternary Operator: " + ternaryResult);

/*--------------------------------------------------------------*/
// Multiple Statement execution
let name;

function isOfLegalAge() {
    name = 'John';
    return 'You are of the legal age limit';
}

function isUnderAge() {
    name = 'Jane';
    return 'You are under the age limit';
}

// The "parseInt" function converts the input recieve into a number data type.

let age = parseInt(prompt("What is your age?"));
console.log(age);
let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);

// parseFloat po kapag gusto mo may decimal.
/*-------------------------------------------------------------*/

// [SECTION] Switch Statement
/*
	- the switch statements evaluates an expression and matches the expression's value to a case clause
	The switch will then execute the statements associated with that case, as well as statements in cases that follow the matching case.
	- Can be used an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected output.
	- The ".toLowerCase()" function/method will change input received fron the prompt into all lowercase letters ensuring a match with the switch case conditions if the user inputs capitalized or uppercase letters.

*/

/*
	switch (expression) {
		case value:
		statement;
		break;
		dafault: statement;
	}

 */

 // Example:

 let day = prompt("What day of the week is it today?").toLowerCase();
 console.log(day);

switch (day) {
    case 'monday': 
        console.log("The color of the day is red");
        break;
    case 'tuesday':
        console.log("The color of the day is orange");
        break;
    case 'wednesday':
        console.log("The color of the day is yellow");
        break;
    case 'thursday':
        console.log("The color of the day is green");
        break;
    case 'friday':
        console.log("The color of the day is blue");
        break;
    case 'saturday':
        console.log("The color of the day is indigo");
        break;
    case 'sunday':
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day");
        break;


 /*
"Please input a valid day"
is parang siya yung else
else statement
assurance
safe zone.
- need merong break.
 */
}


